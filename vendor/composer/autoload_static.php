<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit937e1f9ec3307a5b528c04604091e2f6
{
    public static $prefixLengthsPsr4 = array (
        'Z' => 
        array (
            'Zhaoyang\\' => 9,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Zhaoyang\\' => 
        array (
            0 => __DIR__ . '/../..' . '/src',
        ),
    );

    public static $classMap = array (
        'Composer\\InstalledVersions' => __DIR__ . '/..' . '/composer/InstalledVersions.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit937e1f9ec3307a5b528c04604091e2f6::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit937e1f9ec3307a5b528c04604091e2f6::$prefixDirsPsr4;
            $loader->classMap = ComposerStaticInit937e1f9ec3307a5b528c04604091e2f6::$classMap;

        }, null, ClassLoader::class);
    }
}
